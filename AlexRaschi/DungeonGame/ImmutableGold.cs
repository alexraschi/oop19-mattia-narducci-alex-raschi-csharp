﻿using dungeon.item;
using dungeon.point;
using dungeon.template;

namespace dungeon.gold
{
    public sealed class ImmutableGold : GenericItem, ITemplate<IGold>, IGold
    {
        public int Frequency { get; }
        public int Amount { get; }

        public ImmutableGold(int frequency, int amount) : base("Gold", "Some gold coins", false, Cell.GOLD)
        {
            Frequency = Utilities.RequireFrequency(frequency);
            Amount = Utilities.RequirePositive(amount);
        }

        public IGold GetInstance(IPoint point)
        {
            return new ImmutableGold(Frequency, Amount);
        }
    }
}
