﻿namespace dungeon.attack
{
    public interface IAttack
    {
        Result Result { get; }
        int Damage { get; }
        string Description { get; }
    }

    public enum Result
    {
        INVALID,
        NO_AMMO,
        HIT,
        MISS
    }
}
