﻿using System;
using NUnit.Framework;
using dungeon.point;

namespace dungeon.test
{
    [TestFixture]
    public sealed class TestPoint
    {
        [Test]
        public void TestNew()
        {
            IPoint point = new ImmutablePoint(4, 7);
            IPoint other = new ImmutablePoint(new ImmutablePoint(4, 7));

            Assert.AreEqual(point, other);
            Assert.AreEqual(point.X, 4);
            Assert.AreEqual(point.Y, 7);
        }

        [Test]
        public void TestNewException()
        {
            Assert.Throws<ArgumentException>(() => new ImmutablePoint(null));
        }

        [Test]
        public void TestMove()
        {
            IPoint point = new ImmutablePoint(6, 1);
            IPoint moved = point.Move(3, 2);

            Assert.AreNotEqual(point, moved);
            Assert.AreEqual(moved, new ImmutablePoint(9, 3));
            Assert.AreEqual(moved, point.Move(new ImmutablePoint(3, 2)));
        }

        [Test]
        public void TestMoveException()
        {
            Assert.Throws<ArgumentException>(() => new ImmutablePoint(1, 2).Move(null));
        }

        [Test]
        public void TestInvert()
        {
            Assert.AreEqual(new ImmutablePoint(8, 3).Invert(), new ImmutablePoint(-8, -3));
        }

        [Test]
        public void TestDirection()
        {
            IPoint point = new ImmutablePoint(5, 3);

            Assert.AreEqual(point.GetDirection(point.Move(-3, 0)), Direction.UP);
            Assert.AreEqual(point.GetDirection(point.Move(0, -6)), Direction.LEFT);
            Assert.AreEqual(point.GetDirection(point.Move(10, 0)), Direction.DOWN);
            Assert.AreEqual(point.GetDirection(point.Move(0, 1)), Direction.RIGHT);
        }

        [Test]
        public void TestDirectionException()
        {
            IPoint point = new ImmutablePoint(23, 5);

            Assert.Throws<ArgumentException>(() => point.GetDirection(null));
            Assert.Throws<ArgumentException>(() => point.GetDirection(new ImmutablePoint(point)));
            Assert.Throws<ArgumentException>(() => point.GetDirection(point.Move(34, 1)));
        }

        [Test]
        public void TestOperators()
        {
            ImmutablePoint point = new ImmutablePoint(1, 1);
            ImmutablePoint other = new ImmutablePoint(2, 3);

            Assert.AreEqual(point + other, new ImmutablePoint(3, 4));
            Assert.AreEqual(point - other, new ImmutablePoint(-1, -2));
            Assert.AreEqual(point * other, new ImmutablePoint(2, 3));
            Assert.AreEqual(point / other, new ImmutablePoint(0, 0));
        }

        [Test]
        public void TestOperatorsExceptions()
        {
            ImmutablePoint point = new ImmutablePoint(7, 4);
            ImmutablePoint other;

            Assert.Throws<ArgumentException>(() => other = point + null);
            Assert.Throws<ArgumentException>(() => other = null + point);
            Assert.Throws<ArgumentException>(() => other = point - null);
            Assert.Throws<ArgumentException>(() => other = null - point);
            Assert.Throws<ArgumentException>(() => other = point * null);
            Assert.Throws<ArgumentException>(() => other = null * point);
            Assert.Throws<ArgumentException>(() => other = point / null);
            Assert.Throws<ArgumentException>(() => other = null / point);
        }
    }
}
