﻿namespace dungeon.item
{
    public abstract class GenericItem : IItem
    {
        public string Name { get; }
        public string Description { get; }
        public bool Blocking { get; }
        public Cell Cell { get; }

        protected GenericItem(string name, string description, bool blocking, Cell cell)
        {
            Name = Utilities.RequireNonEmpty(name);
            Description = Utilities.RequireNonEmpty(description);
            Blocking = blocking;
            Cell = cell;
        }
    }
}
