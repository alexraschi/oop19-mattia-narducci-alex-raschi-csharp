﻿using System;
using NUnit.Framework;
using dungeon.gold;
using dungeon.template;

namespace dungeon.test
{
    [TestFixture]
    public sealed class TestGold
    {
        [Test]
        public void TestNew()
        {
            ITemplate<IGold> template = new ImmutableGold(72, 10921);
            IGold gold = template.GetInstance(null);

            Assert.AreNotEqual(template.GetInstance(null), gold);
            Assert.AreEqual(template.Frequency, 72);
            Assert.AreEqual(gold.Amount, 10921);
        }

        [Test]
        public void TestNewExceptions()
        {
            Assert.Throws<ArgumentException>(() => new ImmutableGold(23, -202));
            Assert.Throws<ArgumentException>(() => new ImmutableGold(-10, 64));
        }
    }
}
