﻿using dungeon.point;
using dungeon.template;

namespace dungeon.item
{
    public sealed class ImmutableItem : GenericItem, ITemplate<IItem>
    {
        public int Frequency { get; }

        public ImmutableItem(string name, string description, bool blocking, Cell cell, int frequency) : base(name, description, blocking, cell)
        {
            Frequency = Utilities.RequireFrequency(frequency);
        }

        public IItem GetInstance(IPoint point)
        {
            return new ImmutableItem(Name, Description, Blocking, Cell, Frequency);
        }
    }
}
