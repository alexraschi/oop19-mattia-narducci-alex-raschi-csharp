﻿namespace dungeon.item
{
    public interface IItem
    {
        string Name { get; }
        string Description { get; }
        bool Blocking { get; }
        Cell Cell { get; }
    }
}