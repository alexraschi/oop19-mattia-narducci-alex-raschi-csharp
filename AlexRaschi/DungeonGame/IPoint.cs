﻿namespace dungeon.point
{
    public interface IPoint
    {
        int X { get; }
        int Y { get; }
        IPoint Move(int x, int y);
        IPoint Move(IPoint point);
        IPoint Invert();
        Direction GetDirection(IPoint point);
    }
}