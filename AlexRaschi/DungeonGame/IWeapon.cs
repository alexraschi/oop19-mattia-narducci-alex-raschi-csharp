﻿using dungeon.attack;
using dungeon.item;
using dungeon.point;

namespace dungeon.weapon
{
    public interface IWeapon : IItem
    {
        int Range { get; }
        int Ammo { get; }
        int ExtraDamage { set; }
        IAttack Hit(string striker, IPoint from, IPoint to);
    }
}
