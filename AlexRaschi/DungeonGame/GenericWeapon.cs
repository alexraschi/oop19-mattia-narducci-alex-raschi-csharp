﻿using System;
using dungeon.attack;
using dungeon.item;
using dungeon.point;

namespace dungeon.weapon
{
    public abstract class GenericWeapon : GenericItem, IWeapon
    {
        public int Range { get; }
        public abstract int Ammo { get; protected set; }
        public abstract int ExtraDamage { protected get; set; }
        
        protected GenericWeapon(string name, string description, bool blocking, Cell cell, int range) : base(name, description, blocking, cell)
        {
            Range = Utilities.RequirePositive(range);
        }

        protected bool IsInRange(IPoint from, IPoint to)
        {
            Utilities.RequireNonNull(from);
            Utilities.RequireNonNull(to);

            IPoint distance = from.Move(to.Invert());

            return Math.Abs(distance.X) <= Range && Math.Abs(distance.Y) <= Range;
        }

        public abstract IAttack Hit(string striker, IPoint from, IPoint to);
    }
}
