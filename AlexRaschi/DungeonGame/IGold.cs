﻿using dungeon.item;

namespace dungeon.gold
{
    public interface IGold : IItem
    {
        int Amount { get; }
    }
}
