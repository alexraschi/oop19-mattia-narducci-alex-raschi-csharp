﻿namespace dungeon.item
{
    public abstract class ItemDecorator : IItem
    {
        private readonly IItem Item;

        public string Name => Item.Name;
        public string Description => Item.Description;
        public bool Blocking => Item.Blocking;
        public Cell Cell => Item.Cell;

        protected ItemDecorator(IItem item)
        {
            Item = Utilities.RequireNonNull(item);
        }
    }
}
