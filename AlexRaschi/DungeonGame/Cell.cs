﻿namespace dungeon
{
    public enum Cell
    {
        BOW,
        DOOR,
        EXIT,
        GOLD,
        MOB,
        PLAYER,
        SWORD,
        WALL,
        BOSS
    }
}
