﻿namespace dungeon
{
    public enum Direction
    {
        UP,
        LEFT,
        DOWN,
        RIGHT
    }
}
