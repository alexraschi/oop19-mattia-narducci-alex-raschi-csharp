﻿using dungeon.attack;
using dungeon.item;
using dungeon.point;

namespace dungeon.weapon
{
    public abstract class WeaponDecorator : ItemDecorator, IWeapon
    {
        private readonly IWeapon Weapon;

        public int Range => Weapon.Range;
        public int Ammo => Weapon.Ammo;
        public int ExtraDamage { set => Weapon.ExtraDamage = value; }

        protected WeaponDecorator(IWeapon weapon) : base(weapon)
        {
            Weapon = Utilities.RequireNonNull(weapon);
        }

        public IAttack Hit(string striker, IPoint from, IPoint to) => Weapon.Hit(striker, from, to);
    }
}
