﻿using System;

namespace dungeon
{
    public static class Utilities
    {
        public static T RequireNonNull<T>(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentException("object must be not null");
            }

            return obj;
        }

        public static string RequireNonEmpty(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                throw new ArgumentException("string must be not null and not empty");
            }

            return str;
        }

        public static int RequirePositive(int number)
        {
            if (number < 0)
            {
                throw new ArgumentException("number must be positive");
            }

            return number;
        }

        public static int RequireGreaterThanZero(int number)
        {
            if (number < 1)
            {
                throw new ArgumentException("number must be greater than zero");
            }

            return number;
        }

        public static int RequireFrequency(int number)
        {
            if (number < 0 || number > 100)
            {
                throw new ArgumentException("number must be in range [0..100]");
            }

            return number;
        }
    }
}
