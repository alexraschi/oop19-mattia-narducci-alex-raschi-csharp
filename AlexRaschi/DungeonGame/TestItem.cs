﻿using System;
using NUnit.Framework;
using dungeon.item;
using dungeon.template;

namespace dungeon.test
{
    [TestFixture]
    public sealed class TestItem
    {
        [Test]
        public void TestNewExceptions()
        {
            Assert.Throws<ArgumentException>(() => new ImmutableItem(null, "a", true, Cell.WALL, 1));
            Assert.Throws<ArgumentException>(() => new ImmutableItem("item", null, false, Cell.DOOR, 56));
            Assert.Throws<ArgumentException>(() => new ImmutableItem("mmh", "ok", true, Cell.EXIT, 395));
        }

        [Test]
        public void TestNew()
        {
            string name = "a name";
            string description = "asd";
            bool blocking = true;
            Cell cell = Cell.GOLD;
            IItem item = new ImmutableItem(name, description, blocking, cell, 0);

            Assert.AreEqual(item.Name, name);
            Assert.AreEqual(item.Description, description);
            Assert.AreEqual(item.Blocking, blocking);
            Assert.AreEqual(item.Cell, cell);
        }

        [Test]
        public void TestTemplate()
        {
            int frequency = 76;
            ITemplate<IItem> template = new ImmutableItem("let's", "test", false, Cell.SWORD, frequency);

            Assert.AreEqual(template.Frequency, frequency);
            Assert.AreNotEqual(template.GetInstance(null), template.GetInstance(null));
        }
    }
}
