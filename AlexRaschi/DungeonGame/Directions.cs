﻿using System;
using dungeon.point;

namespace dungeon
{
    public static class Directions
    {
        private static readonly IPoint UP = new ImmutablePoint(-1, 0);
        private static readonly IPoint LEFT = new ImmutablePoint(0, -1);
        private static readonly IPoint DOWN = new ImmutablePoint(1, 0);
        private static readonly IPoint RIGHT = new ImmutablePoint(0, 1);

        public static IPoint GetModifier(this Direction direction)
        {
            switch (direction)
            {
                case Direction.UP:
                    return UP;
                case Direction.LEFT:
                    return LEFT;
                case Direction.DOWN:
                    return DOWN;
                case Direction.RIGHT:
                    return RIGHT;
                default:
                    throw new ArgumentException();
            }
        }
    }
}
