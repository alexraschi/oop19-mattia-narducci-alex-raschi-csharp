﻿using System;

namespace dungeon.point
{
    public class ImmutablePoint : Tuple<int, int>, IPoint
    {
        private static readonly ImmutablePoint INVERT = new ImmutablePoint(-1, -1);

        public int X => Item1;
        public int Y => Item2;

        public ImmutablePoint(int x, int y) : base(x, y)
        {
        }

        public ImmutablePoint(IPoint point) : this(Utilities.RequireNonNull(point).X, point.Y)
        {
        }

        public IPoint Move(int x, int y) => new ImmutablePoint(X + x, Y + y);
        public IPoint Move(IPoint point) => new ImmutablePoint(X + Utilities.RequireNonNull(point).X, Y + point.Y);
        public IPoint Invert() => this * INVERT;

        public Direction GetDirection(IPoint point)
        {
            if (Equals(Utilities.RequireNonNull(point)))
            {
                throw new ArgumentException("the input points are the same point");
            }

            if (X == point.X)
            {
                if (Y < point.Y)
                {
                    return Direction.RIGHT;
                }
                else
                {
                    return Direction.LEFT;
                }
            }
            else if (Y == point.Y)
            {
                if (X < point.X)
                {
                    return Direction.DOWN;
                }
                else
                {
                    return Direction.UP;
                }
            }
            else
            {
                throw new ArgumentException("invalid direction");
            }
        }

        public static ImmutablePoint operator +(ImmutablePoint first, ImmutablePoint second)
        {
            Utilities.RequireNonNull(first);
            Utilities.RequireNonNull(second);

            return new ImmutablePoint(first.X + second.X, first.Y + second.Y);
        }

        public static ImmutablePoint operator -(ImmutablePoint first, ImmutablePoint second)
        {
            Utilities.RequireNonNull(first);
            Utilities.RequireNonNull(second);

            return new ImmutablePoint(first.X - second.X, first.Y - second.Y);
        }

        public static ImmutablePoint operator *(ImmutablePoint first, ImmutablePoint second)
        {
            Utilities.RequireNonNull(first);
            Utilities.RequireNonNull(second);

            return new ImmutablePoint(first.X * second.X, first.Y * second.Y);
        }

        public static ImmutablePoint operator /(ImmutablePoint first, ImmutablePoint second)
        {
            Utilities.RequireNonNull(first);
            Utilities.RequireNonNull(second);

            return new ImmutablePoint(first.X / second.X, first.Y / second.Y);
        }
    }
}
