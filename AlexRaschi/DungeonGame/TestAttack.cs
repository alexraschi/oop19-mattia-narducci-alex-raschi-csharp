﻿using System;
using NUnit.Framework;
using dungeon.attack;

namespace dungeon.test
{
    [TestFixture]
    public sealed class TestAttack
    {
        [Test]
        public void TestNewInvalid()
        {
            IAttack attack = new ImmutableAttack(Result.INVALID, -1, "monkey", "banana");

            Assert.AreEqual(attack.Result, Result.INVALID);
            Assert.AreEqual(attack.Damage, -1);
            Assert.False(string.IsNullOrEmpty(attack.Description));
        }

        [Test]
        public void TestNewNoAmmo()
        {
            IAttack attack = new ImmutableAttack(Result.NO_AMMO, 3082, "flowerpot", "bazooka");

            Assert.AreEqual(attack.Result, Result.NO_AMMO);
            Assert.AreEqual(attack.Damage, 3082);
            Assert.False(string.IsNullOrEmpty(attack.Description));
        }

        [Test]
        public void TestNewHit()
        {
            IAttack attack = new ImmutableAttack(Result.HIT, 19, "warrior", "sarissa");

            Assert.AreEqual(attack.Result, Result.HIT);
            Assert.AreEqual(attack.Damage, 19);
            Assert.False(string.IsNullOrEmpty(attack.Description));
        }

        [Test]
        public void TestNewMiss()
        {
            IAttack attack = new ImmutableAttack(Result.MISS, -1098, "cheeseburger", "mayonnaise");

            Assert.AreEqual(attack.Result, Result.MISS);
            Assert.AreEqual(attack.Damage, -1098);
            Assert.False(string.IsNullOrEmpty(attack.Description));
        }

        [Test]
        public void TestNewExceptions()
        {
            Assert.Throws<ArgumentException>(() => new ImmutableAttack(Result.HIT, -10, null, "hi again"));
            Assert.Throws<ArgumentException>(() => new ImmutableAttack(Result.INVALID, 0, "nope", null));
        }
    }
}
