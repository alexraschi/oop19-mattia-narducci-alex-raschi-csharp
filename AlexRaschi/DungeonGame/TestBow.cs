﻿using System;
using NUnit.Framework;
using dungeon.attack;
using dungeon.point;
using dungeon.template;
using dungeon.weapon;

namespace dungeon.test
{
    [TestFixture]
    public sealed class TestBow
    {
        [Test]
        public void TestNewExceptions()
        {
            Assert.Throws<ArgumentException>(() => new Bow(null, "maybe", 10, 53, 2, 1));
            Assert.Throws<ArgumentException>(() => new Bow("yes", null, 6, 91, 9, 3));
            Assert.Throws<ArgumentException>(() => new Bow("no", "awesome", -5, 13, 4, 3));
            Assert.Throws<ArgumentException>(() => new Bow("quality", "(tm)", 2, 101, 3, 7));
            Assert.Throws<ArgumentException>(() => new Bow("hjkl", "neatvi", 11, 87, -10, 4));
            Assert.Throws<ArgumentException>(() => new Bow("best", "editor", 138, 99, 1239, -12));
        }

        [Test]
        public void TestTemplate()
        {
            ITemplate<IWeapon> template = new Bow("vortex", "quickscope", 99, 98, 75, 10);

            Assert.AreEqual(template.Frequency, 98);
            Assert.AreNotEqual(template.GetInstance(null), template.GetInstance(null));
        }

        [Test]
        public void TestAmmo()
        {
            IWeapon bow = new Bow("devastator", "best weapon", 1000000, 1, 29732, 2);
            string striker = "doomguy";
            IPoint from = new ImmutablePoint(0, 0);
            IPoint to = new ImmutablePoint(-1, 9);

            Assert.AreEqual(bow.Ammo, 2);

            IAttack first = bow.Hit(striker, from, to);

            Assert.AreEqual(first.Result, Result.HIT);
            Assert.AreEqual(bow.Ammo, 1);

            IAttack second = bow.Hit(striker, from, to);

            Assert.AreEqual(second.Result, Result.HIT);
            Assert.AreEqual(bow.Ammo, 0);

            IAttack last = bow.Hit(striker, from, to);

            Assert.AreEqual(last.Result, Result.NO_AMMO);
            Assert.AreEqual(bow.Ammo, 0);
        }

        [Test]
        public void TestExtraDamage()
        {
            IWeapon bow = new Bow("weak", "weapon", 1, 100, 2, 10);
            IPoint from = new ImmutablePoint(0, 0);
            IPoint to = new ImmutablePoint(1, 1);

            bow.ExtraDamage = 16;
            Assert.AreEqual(bow.Hit("me", from, to).Damage, 18);
            Assert.AreEqual(bow.Hit("you", from, to).Damage, 18);
        }

        [Test]
        public void TestHitExceptions()
        {
            Assert.Throws<ArgumentException>(() => new Bow("new", "this", 2348, 12, 14, 4).Hit(null, new ImmutablePoint(9, 7), new ImmutablePoint(4, 6)));
            Assert.Throws<ArgumentException>(() => new Bow("old", "that", 2345, 96, 21, 5).Hit("yes him", null, new ImmutablePoint(8, 1)));
            Assert.Throws<ArgumentException>(() => new Bow("last", "i hope", 357, 46, 15, 8).Hit("maybe his", new ImmutablePoint(7, 5), null));
        }

        [Test]
        public void TestHit()
        {
            IWeapon bow = new Bow("no", "u", 2, 100, 9, 3);
            IPoint from = new ImmutablePoint(2, 3);

            Assert.AreEqual(Result.HIT, bow.Hit("ok", from, new ImmutablePoint(4, 4)).Result);
            Assert.AreEqual(Result.INVALID, bow.Hit("nopz", from, new ImmutablePoint(2, 6)).Result);
        }
    }
}
