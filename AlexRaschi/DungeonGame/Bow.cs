﻿using dungeon.attack;
using dungeon.point;
using dungeon.template;

namespace dungeon.weapon
{
    public sealed class Bow : GenericWeapon, ITemplate<IWeapon>
    {
        public int Frequency { get; }
        public override int Ammo { get; protected set; }
        public override int ExtraDamage { protected get; set; }

        private readonly int Damage;

        public Bow(string name, string description, int range, int frequency, int damage, int ammo) : base(name, description, false, Cell.BOW, range)
        {
            Frequency = Utilities.RequireFrequency(frequency);
            Damage = Utilities.RequirePositive(damage);
            Ammo = Utilities.RequirePositive(ammo);
            ExtraDamage = 0;
        }

        public IWeapon GetInstance(IPoint point)
        {
            return new Bow(Name, Description, Range, Frequency, Damage, Ammo);
        }

        public override IAttack Hit(string striker, IPoint from, IPoint to)
        {
            Utilities.RequireNonEmpty(striker);
            Utilities.RequireNonNull(from);
            Utilities.RequireNonNull(to);

            Result result;

            if (Ammo == 0)
            {
                result = Result.NO_AMMO;
            }
            else if (IsInRange(from, to))
            {
                result = Result.HIT;
                Ammo--;
            }
            else
            {
                result = Result.INVALID;
            }

            return new ImmutableAttack(result, Damage + ExtraDamage, striker, Name);
        }
    }
}
