﻿using System;
using System.Text;

namespace dungeon.attack
{
    public sealed class ImmutableAttack : IAttack
    {
        public Result Result { get; }
        public int Damage { get; }
        public string Description { get; }

        public ImmutableAttack(Result result, int damage, string striker, string weapon)
        {
            Utilities.RequireNonEmpty(striker);
            Utilities.RequireNonEmpty(weapon);

            Result = result;
            Damage = damage;

            StringBuilder description = new StringBuilder(striker);

            switch (result)
            {
                case Result.INVALID:
                    description.Append(" cannot hit there!");
                    break;

                case Result.NO_AMMO:
                    description.Append("'s ").Append(weapon).Append(" has no ammo!");
                    break;

                case Result.HIT:
                    description.Append(" hit with ").Append(weapon).Append(" (").Append(damage).Append(")");
                    break;

                case Result.MISS:
                    description.Append(" missed with ").Append(weapon);
                    break;

                default:
                    throw new ArgumentException(nameof(result));
            }

            Description = description.ToString();
        }
    }
}
