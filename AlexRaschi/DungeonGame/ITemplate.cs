﻿using dungeon.point;

namespace dungeon.template
{
    public interface ITemplate<T>
    {
        int Frequency { get; }
        T GetInstance(IPoint point);
    }
}