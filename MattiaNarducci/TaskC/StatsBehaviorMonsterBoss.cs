﻿
namespace TaskC
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="TaskC.IStatsBehaviorMonster" />
    public class StatsBehaviorMonsterBoss : IStatsBehaviorMonster
    {
        private static readonly int EXP_LEAVED_MULTIPLIER = 10;
        private static readonly int HP_MODIFIER = 4;
        public int ExpLeavedCalcolator(int level)
        {
            Utilities.RequiredPositive(level);
            return level * EXP_LEAVED_MULTIPLIER;
        }

        public void StatsForLevel(int level, IStats stats)
        {
            Utilities.RequiredPositive(level);
            stats.SetStats(stats.Atk + level,
                stats.Def + level, stats.Hp + HP_MODIFIER * (level - 1));
        }
    }
}
