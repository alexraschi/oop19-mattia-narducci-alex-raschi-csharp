﻿

namespace TaskC
{
    public interface IStatsBehaviorMonster
    {
        /// <summary>
        /// Exps the leaved calcolator.
        /// </summary>
        /// <param name="level">The monster level.</param>
        /// <returns></returns>
        int ExpLeavedCalcolator(int level);

        /// <summary>
        /// Statses for level.
        /// </summary>
        /// <param name="level">The monster level.</param>
        /// <param name="stats">The monster stats.</param>
        void StatsForLevel(int level, IStats stats);
    }
}
