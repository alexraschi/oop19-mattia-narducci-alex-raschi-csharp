﻿using System;


namespace TaskC
{
    public interface ICharacter
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        String Name { get; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        String Description { get; }
        /// <summary>
        /// Gets or sets the stats.
        /// </summary>
        /// <value>
        /// The stats.
        /// </value>
        IStats Stats { get; set; }

        /// <summary>
        /// Gets the health remained.
        /// </summary>
        /// <value>
        /// The health remained.
        /// </value>
        int HealthRemained { get; }

        /// <summary>
        /// Sets the health remained.
        /// </summary>
        /// <param name="damage">The damage.</param>
        void SetHealthRemained(int damage);

        /// <summary>
        /// Gets the character.
        /// </summary>
        /// <param name="halthReamained">The halth reamained.</param>
        /// <returns></returns>
        ICharacter GetCharacter(int halthReamained);

        /// <summary>
        /// Determines whether this instance is blocking.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is blocking; otherwise, <c>false</c>.
        /// </returns>
        bool IsBlocking();

        /// <summary>
        /// Determines whether this instance is dead.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is dead; otherwise, <c>false</c>.
        /// </returns>
        bool IsDead();
    }
}
