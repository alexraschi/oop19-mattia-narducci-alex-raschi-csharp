﻿using System;


/// <summary>
/// The Character
/// </summary>
namespace TaskC
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="TaskC.ICharacter" />
    public class Character : ICharacter
    {

        private int healthRemained;

        /// <summary>
        /// Initializes a new instance of the <see cref="Character"/> class.
        /// </summary>
        /// <param name="stats">The stats.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        public Character(IStats stats, String name, String description)
        {
            Utilities.ObjectNonNull(stats);
            Utilities.ObjectNonNull(name);
            Utilities.ObjectNonNull(description);
            Name = name;
            Stats = stats;
            Description = description;
            healthRemained = stats.Hp;

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Character" /> class.
        /// </summary>
        /// <param name="stats">The stats.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="healthRemained">The health remained.</param>
        public Character(IStats stats, String name, String description, int healthRemained) : this(stats, name, description)
        {
            this.healthRemained = healthRemained;

        }

        public string Name { get; }
        public string Description { get; }

        public IStats Stats { get; set; }

        public int HealthRemained { get => healthRemained; }

        public ICharacter GetCharacter(int healthReamained)
        {
            return new Character(Stats, Name, Description, healthReamained);
        }

        public bool IsBlocking()
        {
            return true;
        }

        public bool IsDead()
        {
            if (HealthRemained <= 0)
            {
                return true;
            }
            return false;
        }

        public void SetHealthRemained(int damage)
        {
            Utilities.RequiredZeroOrPositive(damage);
            healthRemained = HealthRemained - damage;
        }
    }
}

