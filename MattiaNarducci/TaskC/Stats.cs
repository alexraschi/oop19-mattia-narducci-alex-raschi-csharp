﻿
namespace TaskC
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="TaskC.IStats" />
    public class Stats : IStats
    {

        private int atk;
        private int def;
        private int hp;

        public Stats(int atk, int def, int hp)
        {
            Atk = atk;
            Def = def;
            Hp = hp;
        }

        public int Def
        {
            get
            {
                return def;
            }
            set
            {
                Utilities.RequiredPositive(value);
                def = value;
            }
        }
        public int Atk
        {
            get
            {
                return atk;
            }
            set
            {
                Utilities.RequiredPositive(value);
                atk = value;
            }
        }
        public int Hp
        {
            get
            {
                return hp;
            }
            set
            {
                Utilities.RequiredPositive(value);
                hp = value;
            }
        }
        public void SetStats(int atk, int def, int hp)
        {
            Atk = atk;
            Def = def;
            Hp = hp;
        }

        public override string ToString() => "HP: " + Hp
                                                 + "\n ATK: " + Atk
                                                 + "\n DEF: " + Def + "\n";
    }
}

