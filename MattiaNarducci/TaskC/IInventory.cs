﻿using System;
using System.Collections.Generic;


namespace TaskC
{
    public interface IInventory
    {
        /// <summary>
        /// Gets the gold.
        /// </summary>
        /// <value>
        /// The gold.
        /// </value>
        int Gold { get; }

        /// <summary>
        /// Takes the weapon.
        /// </summary>
        /// <param name="weapon">The weapon.</param>
        void TakeWeapon(string weapon);

        /// <summary>
        /// Gets the weapon inventory.
        /// </summary>
        /// <value>
        /// The weapon inventory.
        /// </value>
        List<String> WeaponInventory { get; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="IInventory"/> is open.
        /// </summary>
        /// <value>
        ///   <c>true</c> if open; otherwise, <c>false</c>.
        /// </value>
        Boolean Open { get; }

        /// <summary>
        /// Adds the gold.
        /// </summary>
        /// <param name="gold">The gold.</param>
        void AddGold(int gold);

        /// <summary>
        /// Checks the size.
        /// </summary>
        /// <returns></returns>
        Boolean CheckSize();

        /// <summary>
        /// Gets the nelement.
        /// </summary>
        /// <returns></returns>
        int GetNelement();

        /// <summary>
        /// Sets the open.
        /// </summary>
        void SetOpen();
    }
}
