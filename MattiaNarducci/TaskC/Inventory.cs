﻿using System;
using System.Collections.Generic;


namespace TaskC
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="TaskC.IInventory" />
    public class Inventory : IInventory
    {
        private static readonly int BAG_SIZE = 4;
        private int gold;
        private Boolean open;
        public Inventory()
        {
            gold = 0;
            WeaponInventory = new List<String>();
            open = false;
        }

        public int Gold { get => gold; }
        
        public bool Open { get => open; }
        public List<string> WeaponInventory { get; }

        public bool CheckSize()
        {
            if (WeaponInventory.Count >= BAG_SIZE)
            {
                return false;
            }
            return true;
        }

        public int GetNelement()
        {
            return WeaponInventory.Count;
        }

        public void TakeWeapon(string weapon)
        {
            if (CheckSize())
            {
                WeaponInventory.Add(weapon);
            }
        }

        public void AddGold(int value)
        {
            Utilities.RequiredPositive(value);
            gold += value;
        }

        public void SetOpen() => open = !open;
    }
}
