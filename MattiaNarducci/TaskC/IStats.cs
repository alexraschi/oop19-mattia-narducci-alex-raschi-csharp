﻿

namespace TaskC
{
    public interface IStats
    {
        /// <summary>
        /// Gets or sets the definition.
        /// </summary>
        /// <value>
        /// The definition.
        /// </value>
        int Def { get; set; }

        /// <summary>
        /// Gets or sets the atk.
        /// </summary>
        /// <value>
        /// The atk.
        /// </value>
        int Atk { get; set; }

        /// <summary>
        /// Gets or sets the hp.
        /// </summary>
        /// <value>
        /// The hp.
        /// </value>
        int Hp { get; set; }

        /// <summary>
        /// Sets the stats.
        /// </summary>
        /// <param name="atk">The atk.</param>
        /// <param name="def">The definition.</param>
        /// <param name="hp">The hp.</param>
        void SetStats(int atk, int def, int hp);
    }
}
