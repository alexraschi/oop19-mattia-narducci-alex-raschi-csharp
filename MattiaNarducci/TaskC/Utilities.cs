﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskC
{
    class Utilities
    {
        /// <summary>
        /// Check if object is null
        /// </summary>
        /// <param name="o">The o.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void ObjectNonNull(Object o)
        {
            if (o == null)
            {
                throw new ArgumentNullException();
            }
        }

        /// <summary>
        /// Check if x is > 0
        /// </summary>
        /// <param name="x">The x value.</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static void RequiredPositive(int x)
        {
            if (x <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Requireds x equal to zero or greater.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static void RequiredZeroOrPositive(int x)
        {
            if (x < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
        }
    }
}
