﻿
namespace TaskC
{
    public interface IStatsBehaviorPlayer
    {
        /// <summary>
        /// Exps to level up.
        /// </summary>
        /// <param name="level">The player level.</param>
        /// <returns></returns>
        int ExpToLevelUp(int level);

        /// <summary>
        /// Modify stats due to level up
        /// </summary>
        /// <param name="stats">The player stats.</param>
        void StatsLevelUp(IStats stats);
    }
}
