﻿
namespace TaskC
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="TaskC.IStatsBehaviorPlayer" />
    public class StatsBehaviorPlayer : IStatsBehaviorPlayer
    {
        private static readonly int HP_MODIFIER = 5;
        private static readonly int EXP_LEVEL_UP = 10;

        public int ExpToLevelUp(int level)
        {
            Utilities.RequiredPositive(level);
            return level * EXP_LEVEL_UP;
        }

        public void StatsLevelUp(IStats stats)
        {
            stats.SetStats(stats.Atk + 1,
                stats.Def + 1, stats.Hp + HP_MODIFIER);
        }
    }
}
