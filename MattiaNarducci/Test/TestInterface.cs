﻿using System;
using TaskC;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class UnitTest3
    {
        private readonly IInventory inventory = new Inventory();

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestMoneyNoValid() => inventory.AddGold(-1);

        [TestMethod]
        public void TestOpenInventory()
        {
            inventory.SetOpen();
            Assert.AreEqual(inventory.Open, true);
            inventory.SetOpen();
            Assert.AreEqual(inventory.Open, false);
        }

        [TestMethod]
        public void TestInventory()
        {
            inventory.TakeWeapon("a");
            Assert.IsTrue(inventory.WeaponInventory.Contains("a"));
            inventory.TakeWeapon("a");
            inventory.TakeWeapon("a");
            inventory.TakeWeapon("a");
            Assert.IsTrue(inventory.GetNelement() == 4);
            inventory.TakeWeapon("a");
            Assert.IsTrue(inventory.GetNelement() == 4);
        }
    }
}
