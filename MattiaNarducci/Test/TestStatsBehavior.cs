﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskC;
namespace Test
{
    [TestClass]
    public class TestStatsBehavior
    {
        private readonly IStatsBehaviorMonster statsBoss = new StatsBehaviorMonsterBoss();
        private readonly IStatsBehaviorMonster statsMedium = new StatsBehaviorMonsterMedium();
        private readonly IStatsBehaviorPlayer statsPlayer = new StatsBehaviorPlayer();
        private IStats stats = new Stats(2, 2, 2);

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestBossLevelNoValid() => statsBoss.ExpLeavedCalcolator(0);

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestMediumLevelNoValid() => statsMedium.ExpLeavedCalcolator(0);

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestPlayerLevelNoValid() => statsPlayer.ExpToLevelUp(0);

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestStatsLevelNoValid() => statsMedium.StatsForLevel(0, stats);

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestStatsBossLevelNoValid() => statsBoss.StatsForLevel(0, stats);

        [TestMethod]
        public void TestStatsBehaviorPlayer()
        {
            Assert.AreEqual(statsPlayer.ExpToLevelUp(5), 50);
            statsPlayer.StatsLevelUp(stats);
            Assert.IsTrue(stats.Atk == 3 && stats.Def == 3 && stats.Hp == 7);
        }

        [TestMethod]
        public void TestStatsBehaviorMonster()
        {
            Assert.AreEqual(statsBoss.ExpLeavedCalcolator(5), 50);
            statsBoss.StatsForLevel(1,stats);
            Assert.IsTrue(stats.Atk == 3 && stats.Def == 3 && stats.Hp == 2);
        }

    }
}
