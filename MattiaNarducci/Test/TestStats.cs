﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskC;
namespace Test
{
    [TestClass]
    public class TestStats
    {

        private readonly IStats stats = new Stats(6, 6, 6);
        private readonly int damage = 3;

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestHpNoValid()
        {
            new Stats(2, 2, -1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestAtkNoValid()
        {
            new Stats(-1, 5, 5);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestDefNoValid()
        {
            new Stats(5, -1, 5);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestSetStatsNoValid()
        {
            stats.SetStats(-1, 5, 8);
        }

        [TestMethod]
        public void TestStatsGeneral()
        {
            Assert.AreEqual(stats.Atk, 6);
            Assert.AreEqual(stats.Hp, 6);
            Assert.AreEqual(stats.Def, 6);
            stats.Def = 2;
            stats.Atk = 4;
            stats.Hp = 3;
            Assert.AreEqual(stats.Atk, 4);
            Assert.AreEqual(stats.Hp, 3);
            Assert.AreEqual(stats.Def, 2);
            stats.SetStats(7, 8, 9);
            Assert.AreEqual(stats.Atk, 7);
            Assert.AreEqual(stats.Hp, 9);
            Assert.AreEqual(stats.Def, 8);
        }
    }
}
