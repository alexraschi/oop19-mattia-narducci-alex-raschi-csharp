using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskC;
namespace Test
{
    [TestClass]
    public class UnitTest1
    {

        private readonly Character breed = new Character(new Stats(6, 6, 6), "a", "b");
        private readonly int damage = 3;

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestMethod1()
        {
            new Character(null, "a", "bn");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestMethod2()
        {
            new Character(new Stats(5,5,5), null, "bn");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestMethod3()
        {
            new Character(new Stats(5, 5, 5), "a", null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestMethod4()
        {
            breed.SetHealthRemained(-1);
        }

        [TestMethod]
        public void TestCharacterGeneral()
        {
            Assert.AreEqual(breed.Name, "a");
            Assert.AreEqual(breed.Description, "b");
            //Assert.AreEqual(breed.Stats, new Stats(6, 6, 6));
            breed.SetHealthRemained(damage);
            Assert.IsFalse(breed.IsDead());
            Assert.AreEqual(breed.HealthRemained, 3);
            breed.SetHealthRemained(damage);
            Assert.IsTrue(breed.IsDead());
        }
    }
}
